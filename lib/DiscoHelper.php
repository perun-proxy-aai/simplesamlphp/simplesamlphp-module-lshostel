<?php

namespace SimpleSAML\Module\lshostel;

use SimpleSAML\Utils\HTTP;

class DiscoHelper
{
    public static function searchScript()
    {
        return '<script type="text/javascript">

	$(document).ready(function() { 
		$("#query").liveUpdate("#list");
	});
	
	</script>';
    }

    /**
     * @param sspmod_perun_DiscoTemplate $t
     * @param array $metadata
     * @param bool $favourite
     *
     * @return string html
     */
    public static function showEntry($t, $metadata, $favourite = false)
    {
        if (isset($metadata['tags']) && in_array('social', $metadata['tags'], true)) {
            return self::showEntrySocial($t, $metadata);
        }

        $extra = ($favourite ? ' favourite' : '');
        $html = '<a class="metaentry' . $extra . ' list-group-item" href="' . $t->getContinueUrl(
            $metadata['entityid']
        ) . '">';

        $html .= '<strong>' . $t->getTranslatedEntityName($metadata) . '</strong>';

        $html .= self::showIcon($metadata);

        $html .= '</a>';

        return $html;
    }

    /**
     * @param sspmod_perun_DiscoTemplate $t
     * @param array $metadata
     *
     * @return string html
     */
    public static function showEntrySocial($t, $metadata)
    {
        $bck = 'white';
        if (!empty($metadata['color'])) {
            $bck = $metadata['color'];
        }

        $html = '<a class="btn btn-block social" href="' . $t->getContinueUrl(
            $metadata['entityid']
        ) . '" style="background: ' . $bck . '">';

        $html .= '<img src="' . $metadata['icon'] . '">';

        $html .= '<strong>Sign in with ' . $t->getTranslatedEntityName($metadata) . '</strong>';

        $html .= '</a>';

        return $html;
    }

    /**
     * Show icon for IdP.
     * Logos are turned off by default, because they are loaded via URL from IdP.
     * Some IdPs have bad configuration, so it breaks the WAYF.
     * @param $metadata
     * @param $logos
     * @return string
     */
    public static function showIcon($metadata, $logos = false)
    {
        if ($logos) {
            if (isset($metadata['UIInfo']['Logo'][0]['url'])) {
                return '<img src="' . htmlspecialchars(HTTP::resolveURL($metadata['UIInfo']['Logo'][0]['url'])) .
                    '" class="idp-logo">';
            }
            if (isset($metadata['icon'])) {
                return '<img src="' . htmlspecialchars(HTTP::resolveURL($metadata['icon'])) . '" class="idp-logo">';
            }
        }
        return '';
    }
}
