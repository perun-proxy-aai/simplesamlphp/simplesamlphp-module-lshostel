# lshostel-aai-proxy-idp-template

![maintenance status: end of life](https://img.shields.io/maintenance/end%20of%20life/2023)

This project has reached end of life, and has been superseeded by the project [simplesamlphp-module-lsaai](https://gitlab.ics.muni.cz/perun-proxy-aai/simplesamlphp/simplesamlphp-module-lsaai)

## Description

Template for LifeScience Hostel AAI Proxy IdP component

## Instalation

```sh
php composer.phar require bbmri/simplesamlphp-module-lshostel
```
